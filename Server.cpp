#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <vector>


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	std::cout << "Waiting for client connection request" << std::endl;
	std::vector<std::thread> threads;
	for (;;)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		// notice that we step out to the global namespace
		// for the resolution of the function accept
		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);
		// the function that handle the conversation with the client
		std::thread t = std::thread(&Server::clientHandler, client_socket);
		threads.push_back(t);
	}
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::cout << "Client accepted. Server and client can speak" << std::endl;
	try
	{
		std::string s = "Login: enter name: ";
		send(clientSocket, s.c_str(), s.size(), 0);  // last parameter: flag. for us will be 0.

		char m[5];
		recv(clientSocket, m, 4, 0);
		m[4] = 0;
		std::cout << "Client name is: " << m << std::endl;
		clientTalker(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}


}void Server::clientTalker(SOCKET clientSocket)
{
	try
	{
		char m[20];
		while (m != "#QUIT#") {
			std::string s = "TALK! :\n";
			send(clientSocket, s.c_str(), s.size(), 0);  // last parameter: flag. for us will be 0.
			char m[20];
			recv(clientSocket, m, 19, 0);
			m[19] = 0;
			std::cout << "<client resp>:: " << m << "\n";
		}
		std::string s = "<<client |DISCONNECTED|>>....";
		send(clientSocket, s.c_str(), s.size(), 0);
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}


}
